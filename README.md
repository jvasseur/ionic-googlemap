# Sample GoogleMaps app

## Getting started

### Install prerequisites

`npm install -g ionic cordova`

### Start the app in development mode

`cd ./myApp`

`ionic cordova run browser -l`

## How was the app created

* Generate project from Ionic CLI

`ionic start myApp sidemenu`

* Generate GoogleMaps API key

https://github.com/ionic-team/ionic-native-google-maps/blob/master/documents/api_key/generate_api_key.md


* Install GoogleMaps plugin

https://docs.google.com/presentation/d/e/2PACX-1vScoho1ensbR4qCI9AIuQN55BZVvK73pAjI7sumDvW3CrxxHnrmpXWUjx2-8CpFibqU1EjLKCRhuthJ/pub?start=false&loop=false&delayms=3000&slide=id.g282d0a7bfd_0_634

https://github.com/ionic-team/ionic-native-google-maps/blob/master/documents/README.md

```
ionic cordova plugin add cordova-plugin-googlemaps \
  --variable API_KEY_FOR_ANDROID=<MY_KEY> \
  --variable API_KEY_FOR_IOS=<MY_KEY>
```

`npm install --save @ionic-native/core@latest @ionic-native/google-maps@latest`

* Install types to support nodejs global variables such as `process`

`npm install "@types/node" --save-dev`

* Build for web browser

`ionic cordova build browser -l`

